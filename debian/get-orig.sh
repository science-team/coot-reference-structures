#!/bin/bash

set -ue

DOWNLOAD_ROOT=https://www2.mrc-lmb.cam.ac.uk/personal/pemsley/coot/dependencies/
DOWNLOAD_BASE=reference-structures
ARCHIVE_SUFFIX=.tar.gz
DOWNLOAD_FILE=${DOWNLOAD_BASE}${ARCHIVE_SUFFIX}

VERSION=$(curl ${DOWNLOAD_ROOT} | grep ${DOWNLOAD_FILE} | grep --only-matching -P '\d{4}-\d{2}-\d{2}' | sed 's/-//g')

curl ${DOWNLOAD_ROOT}${DOWNLOAD_FILE} \
     > ../coot-${DOWNLOAD_BASE}_${VERSION}.orig${ARCHIVE_SUFFIX}
